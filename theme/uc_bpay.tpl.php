<div id="bpay-box" style="border: 2px solid #0C2750; color: #0C2750; float: left; padding: 10px; width: 355px;">
	<img src="<?php global $base_url; print $base_url .'/'. drupal_get_path('module', 'uc_bpay'); ?>/theme/images/bpay_logo.png" alt="BPAY Logo" style="	width: 123px; height: 55px; display: block; float: left;"/>
	<div id="bpay-info" style="	border: 2px solid #0C2750; float: left; padding: 7px; font-size: 14px; font-weight: bold; width: 204px; margin: 0 0 0 10px;">
		<div id="bpay-biller">Biller Code : <?php print $bpay_biller; ?></div>
		<div id="bpay-ref">Ref : <?php print $bpay_crn; ?></div>
	</div>
	<div id="bpay-help" style="float: left; font-size: 12px; line-height: 15px;">
		<h2 style="	color: #0C2750; font-size: 17px; line-height: normal; margin: 6px 0 0 0;">Telephone & Internet Banking – BPAY®</h2>
		<?php if ($accept_cc): ?>
			Contact your bank or financial institution to make this payment from your cheque, savings, debit, credit card or transaction account. More info: www.bpay.com.au
		<?php else:?>
			Contact your bank or financial institution to make this payment from your cheque, savings, debit or transaction account. More info: www.bpay.com.au		
		<?php endif; ?>
	</div>
</div>
