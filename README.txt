-- INSTALLATION - INVOICE TEMPLATE --
Please copy and paste this module under /sites/all/modules/contri/
Then enable it under /admin/modules

once enabled, you can then config it under /admin/store/settings/payment

Please use
$order_bpay-payment-details to display the styled BPAY payment details box on the invoice
$order_bpay_biller_code for Predefined Biller code
$order_bpay_crn for Customer reference number
$order_bpay_accept_cc to check if Credit card is allowed

you can also use the example invoice template under the uc_bpay/example folder.